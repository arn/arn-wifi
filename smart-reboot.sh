#!/bin/ash

FLAG=/etc/no-smart-reboot
IP=89.234.141.73

# See here to understand why this sleep 70
# https://openwrt.org/docs/guide-user/base-system/cron#periodic_reboot_of_a_router
sleep 70
if [ -e $FLAG ]; then
    if ping -c10 -q $IP > /dev/null ; then
        rm -f $FLAG
    elif [ "$(date +%M)" = "42" ]; then
        touch $FLAG
        # Update time at boot
        touch /etc/banner
        reboot
    elif [ "$(date +%M)" = "12" ]; then
        touch $FLAG
        # Update time at boot
        touch /etc/banner
        reboot
    fi
else
    if ! ping -c2 -q $IP > /dev/null ; then
        touch $FLAG
        # Update time at boot
        touch /etc/banner
        reboot
    elif [ "$(date +%H%M)" = "0430" ]; then
        touch $FLAG
        # Update time at boot
        touch /etc/banner
        reboot
    fi
fi
